import java.io.*;
import java.util.Scanner;
import java.util.HashMap; // import the HashMap class
import java.util.*; 
import org.json.simple.JSONArray;


public class matrix {

	static  int MAX_DOC = 10;
	
	
	
	static int numberOfHashFunc = 15;
	
	static int nShinglesForEachDoc = 50;
	static int ngram = 3;
	static int r = 3;
	
	static Boolean [][] mainMatrix = new Boolean[nShinglesForEachDoc*MAX_DOC][MAX_DOC];
	
	static Integer [][] hashMatrix = new Integer[nShinglesForEachDoc*MAX_DOC][numberOfHashFunc];
	
	
	static Integer [][] signatureMatrix = new Integer[numberOfHashFunc][MAX_DOC];
	
	static HashMap<String, Integer> shinglesMap = new HashMap<String, Integer>();
	
	
	static int nHashFunc = 0;
	static int nDcoument = 0;
	static int nShingles = 0;
	
	
	
	
	public static void main(String[] args) {
		
		LSH();
		//int[][] arr = {{1,0,0,0,2}, {3,2,1,2,2}, {0,1,3,1,1}, {2,1,2,2,0}, {4,3,4,2,2}, {0,0,0,1,1}};
		//bandLSH(arr,3,2, 5);
		
	}
	
	public static void LSH(){
		initialize();
		createShingles();
		printMatrix(mainMatrix);
		createHashMatrix(shinglesMap.size(),numberOfHashFunc);
		printMatrix(hashMatrix);
		createSignatureMatrix(nShingles,nHashFunc,nDcoument);
		printMatrix(signatureMatrix);
		bandLSH(signatureMatrix,r,numberOfHashFunc/r,nDcoument);
	}
	
	public static void createShingles(){
		int counter = 0;
		int docId = 0;
		
		
		JSON.JsonParser.parseJson();
		String doc = "demo";
		while(doc != null){
			doc = JSON.JsonParser.getNextString();
			if(doc == null){break;}
			System.out.println(doc);
			
	        
	        doc = doc.replace(' ', '#');
	        System.out.println(doc);
	        if(doc.length()<10){continue;}
	        int[] rndAry = randomArray(doc.length()-ngram,nShinglesForEachDoc);
	    	
	        for(int i = 0; i< rndAry.length;i++){
	        	int index = rndAry[i];
	        	String shingles = doc.substring(index, index + ngram);
	        	if (!shinglesMap.containsKey(shingles)){
	        		
	        		mainMatrix[counter][docId] = new Boolean(true);
	        		shinglesMap.put(shingles, counter);
	        		counter++;
	        		nShingles++;
	        	} else {
	        		int counterPrev = shinglesMap.get(shingles);
	        		mainMatrix[counterPrev][docId] = new Boolean(true);
	        	}
	        	
	        	System.out.println("shingles = " + shingles);
	        }
	        docId++;
	        nDcoument++;
			
		}
	}
	
	public static int[] randomArray(int doclen,int numberOfShingles){
		//java.util.Random r = new java.util.Random();
		//int[] randomArray = r.ints(0, doclen-1).limit(numberOfShingles).toArray();
		int[] randomArray = new int[doclen];
		for (int i = 0; i < doclen; i++){
			randomArray[i] = i;
		}
		return randomArray;
	}
	
	
	public static void createHashMatrix(int numberOfShingles, int numberOfHashFunc){
		nHashFunc = numberOfHashFunc;
		LSH.HG h = new LSH.HG(numberOfShingles);
		
		for (int j = 0; j < numberOfHashFunc; j++){
			int hd = h.getNextHashFunction();
			for (int i = 0; i < numberOfShingles; i++ ){
				int hv = h.getHashValue(hd, i);
				hashMatrix[i][j] = hv;	
				
			}
		}
		 System.out.println();
	}
	
	public static void createSignatureMatrix(int numberOfShingles, int numberOfHashFunc, int numberOfDocument){
		
		//initialize signature matrix
		for (int i = 0; i < numberOfHashFunc; i++){
			for (int j = 0; j < numberOfDocument; j++){
				signatureMatrix[i][j] = Integer.MAX_VALUE; 
			}
		}
		
		for (int i = 0; i < numberOfShingles; i++){
			for (int j = 0; j < numberOfDocument; j++){
				for (int k = 0; k < numberOfHashFunc; k++){
					if(mainMatrix[i][j] != null){
						if(mainMatrix[i][j].booleanValue()){
							if(signatureMatrix[k][j] > hashMatrix[i][k]){
								signatureMatrix[k][j] = hashMatrix[i][k];
							}
						}
					}
				}
			}
		}
		
	}
	
	public static void initialize(){
		//initialize
		nDcoument = 0;
		nShingles = 0;
		nHashFunc = 0;
	}
	

	public static  <T> void  printMatrix(T[][] array){
		for (int i = 0; i < array.length; i++) {
		    for (int j = 0; j < array[0].length; j++) {
		    	System.out.print(array[i][j] + " ");
		    }
		    System.out.println();
		}
	}
		
	public static void bandLSH(Integer [][] sigMat, int r, int b, int numberOfDocId){
		
		 
		
		Vector<HashMap<String, Vector<Integer>>> bandMapAry = new Vector<HashMap<String, Vector<Integer>>>(); 
		
		for(int i=0; i< b; i++){
			HashMap<String, Vector<Integer>> bm = new HashMap<String, Vector<Integer> >();
			for(int j =0; j < numberOfDocId; j++){
				String z = "";
				for(int k =0; k< r; k++){
					Integer u = sigMat[k + r*i][j];
					z = z + sigMat[k + r*i][j].toString(); //Integer.toString(sigMat[k + r*i][j]); // tostring
				}
				if(bm.containsKey(z)){
					Vector<Integer> v = bm.get(z);
					v.add(j);
				} else {
					Vector<Integer> v = new Vector<Integer>();
					bm.put(z, v);
					v.add(j);
				}
			}
			bandMapAry.add(bm);
		}
		 System.out.println();
	}
}


