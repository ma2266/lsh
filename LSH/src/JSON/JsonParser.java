package JSON;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
 
import java.io.FileReader;
import java.util.Iterator;

public class JsonParser {
	
	public static Iterator<JSONObject> iterator;
	
	public static void parseJson() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("D:/eclipse project/LSH/Input/test2.json"));
 
			// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
			JSONArray jsonArray = (JSONArray) obj;
			iterator = jsonArray.iterator();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getNextString(){
		if (iterator.hasNext()) {
			String s = (String)iterator.next().get("short_description");
			return s;
		} 
		return null;
	}
}
