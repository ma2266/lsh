package LSH;

import java.util.Random;
import java.util.Vector;

import javafx.util.Pair;

public class HG {
	
	int maxACoef = 100000;
	int maxBCoef = 100000;
	int modValue = 0;
	
	public HG(int modValue){
		this.modValue = modValue;
	}
	
	Vector<Pair<Integer,Integer>> hashArray = new Vector<Pair<Integer,Integer>>();
	int funtionDescriptor = -1;
	public int  getNextHashFunction (){
		Random rand = new Random();
        int a = rand.nextInt(maxACoef); 
        int b = rand.nextInt(maxBCoef);
        Pair<Integer,Integer> p = new Pair<Integer,Integer>(a,b); 
        hashArray.add(p);
        funtionDescriptor++;
		return funtionDescriptor;
	}
	
	public int getHashValue(int fd,int row){
		Pair<Integer,Integer> p = hashArray.get(fd);
		
		int result = p.getKey()*row + p.getValue();
		
		
		result = result % modValue;
		return result;
	}
	
}
