package CSV;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



public class CSV {
	

	public static void parseCSV() {
		// TODO Auto-generated method stub
		String csvFile = "C:/Users/Mahsa Asadi/Desktop/research senjuti/emails.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            int lineCounter = 0;
            while ((line = br.readLine()) != null && lineCounter < 15) {

                // use comma as separator
                String[] emails = line.split(cvsSplitBy);

                System.out.println("email id = " + emails[0] + "\n Content = " + emails[1] + "]");
                lineCounter++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
